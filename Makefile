.PHONY : build

restore:
	dotnet restore

build: restore
	dotnet build --nologo /clp:NoSummary --no-restore /property:GenerateFullPaths=true

test: build
	dotnet test

clean:
	find . -iname "bin" -o -iname "obj" | xargs rm -rf
