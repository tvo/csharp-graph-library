using System.Collections.Generic;

namespace GraphLibrary
{
	public static class GenericHelper<TV, TE>
	{
		public class Edge : GraphLibrary.Edge<TE, Vertex, TV>
		{
			public Edge(Vertex from, Vertex to) : base(from, to)
			{
			}
			public Edge(Vertex from, Vertex to, TE value) : base(from, to, value)
			{
			}
		}

		public class Vertex : GraphLibrary.Vertex<TV>
		{
			public Vertex(TV value) : base(value)
			{
			}
		}

		public class Graph : GraphLibrary.Graph<Vertex, Edge, TV, TE>
		{
			public Graph(IEnumerable<Vertex> vertices, IEnumerable<Edge> edges) : base(vertices, edges, false, false)
			{
			}
		}

		public class Path : GraphLibrary.Path<Edge, Vertex, TE, TV>
		{
			public Path(IEnumerable<Edge> edges) :base(edges) { }
		}

		public class Tree : GraphLibrary.Tree<Tree, Vertex, Edge, TV, TE>
		{
			public Tree(Vertex root, IEnumerable<ITuple<Edge, Tree>> children = null) : base(root, children)
			{
			}
		}

		public static class Algorithms
		{
			public class DisconnectedGraph : GraphLibrary.Algorithms.DisconnectedGraphLibrary.DisconnectedGraphLibrary<Graph, Vertex, Edge, TV, TE>
			{
				public DisconnectedGraph(Graph graph) : base(graph)
				{
				}
			}

			// the other algorithms don't make sense to add here for now because they require a number for the edge value. something to add later.
		}
	}
}
