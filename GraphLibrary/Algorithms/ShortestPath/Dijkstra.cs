using System;
using System.Collections.Generic;
using System.Linq;

using TVertex = GraphLibrary.GenericHelper<string, int>.Vertex;
using TGraph = GraphLibrary.GenericHelper<string, int>.Graph;
using TEdge = GraphLibrary.GenericHelper<string, int>.Edge;
using TPath = GraphLibrary.GenericHelper<string, int>.Path;

namespace GraphLibrary.Algorithms.ShortestPath
{
	public class Dijkstra
	{
		public TGraph SourceGraph { get; set; }

		public Dijkstra(TGraph graph)
		{
			SourceGraph = graph;
		}

		private Dictionary<TVertex, int?> InitializeDistanceDictionary()
		{
			return SourceGraph.Vertices.ToDictionary(x => x, x => (int?)null);
		}

		private TPath GetPathFromMinGraph(IDictionary<TVertex, int?> distances, TVertex start, TVertex end)
		{
			// To get the solution, we must work backwards from previously calculated solution.
			var edges = new Queue<TEdge>();

			var current = end;
			while(true)
			{
				var inEdges = SourceGraph.IncomingEdges(current).Select(e => Tuple.Create(e, distances[e.From].Value)).ToList();
				var minEdge = inEdges.Where(t => t.Item2 + t.Item1.Value == distances[current].Value).First().Item1;

				current = minEdge.From;
				edges.Enqueue(minEdge);
				if(current == start)
					break;
			}
			
			return new TPath(edges.ToList());
		}
		
		public TPath FindShortestPath(string vertexA, string vertexB)
		{
			var shortAndDistance = FindShortestPathDistanceAndGraph(vertexA, vertexB);
			var a = SourceGraph.VertexOfValue(vertexA);
			var b = SourceGraph.VertexOfValue(vertexB);
			return GetPathFromMinGraph(shortAndDistance.Item2, a, b);
		}

		public int FindShortestPathDistance(string vertexA, string vertexB)
		{
			var (shortest, discard) = FindShortestPathDistanceAndGraph(vertexA, vertexB);

			return shortest;
		}

		private Tuple<int, Dictionary<TVertex, int?>> FindShortestPathDistanceAndGraph(string vertexA, string vertexB)
		{
			var vertex = SourceGraph.VertexOfValue(vertexA);

			var visitedVerticies = new List<TVertex>();
			var partialiallyVisited = new Queue<TVertex>();

			partialiallyVisited.Enqueue(vertex);

			var distances = InitializeDistanceDictionary();
			distances[vertex] = 0;


			while(partialiallyVisited.Any())
			{
				var currentVertex = partialiallyVisited.Dequeue();
				visitedVerticies.Add(currentVertex);

				// probably need to color these or something...
				var connections = SourceGraph.OutgoingEdges(currentVertex).ToList();

				foreach(var edge in connections)
				{
					var to = edge.To;

					if(distances[to] is null || distances[to].Value > distances[currentVertex] + edge.Value)
						distances[to] = distances[currentVertex] + edge.Value;

					if(visitedVerticies.Contains(to) || partialiallyVisited.Contains(to))
						continue;
					partialiallyVisited.Enqueue(to);
				}
			}

			var shortest = distances[SourceGraph.VertexOfValue(vertexB)].GetValueOrDefault();
			return Tuple.Create(shortest, distances);
		}
	}
}
