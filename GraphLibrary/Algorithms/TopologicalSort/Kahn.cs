using System;
using System.Collections.Generic;
using System.Linq;

using TVertex = GraphLibrary.GenericHelper<string, int>.Vertex;
using TGraph = GraphLibrary.GenericHelper<string, int>.Graph;

namespace GraphLibrary.Algorithms.TopologicalSort
{
	public class Kahn
	{
		public TGraph SourceGraph { get; set; }

		public Kahn(TGraph graph)
		{
			SourceGraph = graph;
		}

		private Dictionary<TVertex, int> InitializeDistanceDictionary()
		{
			return SourceGraph.Vertices.ToDictionary(x => x, x => SourceGraph.IncomingEdges(x).Count());
		}

		private class MutableTuple<TA, TB>
		{
			public MutableTuple(TA ta, TB tb)
			{
				Item1 = ta;
				Item2 = tb;
			}

			public TA Item1 { get; set; }
			public TB Item2 { get; set; }
		}

		public List<TVertex> Sort()
		{
			var verticiesWithCount = InitializeDistanceDictionary()
				.OrderBy(x => x.Value).Select(x => new MutableTuple<TVertex, int>(x.Key, x.Value)).ToList();
			var result = new List<TVertex>();

			while(verticiesWithCount.Any(v => v.Item2 == 0))
			{
				var tup = verticiesWithCount.First(x => x.Item2 == 0);
				var selected = tup.Item1;
				verticiesWithCount.Remove(tup);
				result.Add(selected);
				foreach(var incomingEdge in SourceGraph.OutgoingEdges(selected))
				{
					var decTup = verticiesWithCount.First(x => x.Item1 == incomingEdge.To);
					--decTup.Item2;
				}
			}

			if(verticiesWithCount.Any())
				throw new Exception("There are cycles in the graph. Can't perform topological sort on a graph with cycles.");

			return result;
		}
	}
}
