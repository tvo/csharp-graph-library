using System;
using System.Collections.Generic;
using System.Linq;

using TVertex = GraphLibrary.GenericHelper<string, int>.Vertex;
using TGraph = GraphLibrary.GenericHelper<string, int>.Graph;
using TEdge = GraphLibrary.GenericHelper<string, int>.Edge;

namespace GraphLibrary.Algorithms.MinimumSpanningTree
{
    public class Krushkal
    {
        public TGraph SourceGraph { get; }

        public Krushkal(TGraph graph)
        {
            SourceGraph = graph;
        }


        private Tuple<List<TVertex>, List<TVertex>> FindDisjointedBetweenEdge(TEdge edge, IEnumerable<List<TVertex>> disjointedSets)
		{
			List<TVertex> fromList = null, toList = null;
			foreach(var set in disjointedSets)
			{
				foreach(var v in set)
				{
					if(fromList is null && v == edge.From)
					{
						fromList = set;
						break;
					}
					if(toList is null && v == edge.To)
					{
						toList = set;
						break;
					}
				}
				if(fromList != null && toList != null)
					break;
			}
			return Tuple.Create(fromList, toList);
		}
		
		public TGraph MinSpanningTree()
        {
			// edges is a sorted list by min edge weight
            var edges = SourceGraph.Edges.OrderBy(e => e.Value).ToList();
            var minEdges = new List<TEdge>();

            // every vetex is at first considered disjointed.
            var disjointed = new List<List<TVertex>>();
            foreach (var v in SourceGraph.Vertices)
                disjointed.Add(new List<TVertex>() { v });

			while(disjointed.Count > 1)
			{
				// select min edge and join the two disjoint sets
				var edge = edges[0];
				minEdges.Add(edge);
				edges.RemoveAt(0);
				var twoSets = FindDisjointedBetweenEdge(edge, disjointed);
				disjointed.Remove(twoSets.Item1);
				disjointed.Remove(twoSets.Item2);
				var joined = new List<TVertex>();
				joined.AddRange(twoSets.Item1);
				joined.AddRange(twoSets.Item2);
				disjointed.Add(joined);
			}
            return new TGraph(disjointed[0], minEdges);
        }
    }
}
