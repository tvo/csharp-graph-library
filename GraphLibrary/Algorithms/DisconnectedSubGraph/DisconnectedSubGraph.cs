using System.Collections.Generic;
using System.Linq;
using GraphLibrary.Interfaces;

namespace GraphLibrary.Algorithms.DisconnectedGraphLibrary
{
	public class DisconnectedGraphLibrary<TGraph, TVertex, TEdge, TVertexValue, TEdgeValue>
		where TGraph : IGraph<TVertex, TEdge, TVertexValue, TEdgeValue>
		where TVertex : IVertex<TVertexValue>
		where TEdge : IEdge<TEdgeValue, TVertex, TVertexValue>
	{
		public TGraph SourceGraph { get; set; }

		public DisconnectedGraphLibrary(TGraph graph)
		{
			SourceGraph = graph;
		}

		private List<GenericHelper<TVertexValue, TEdgeValue>.Graph> SortUtil(List<TVertex> verticies, List<TEdge> edges)
		{
			var resultSubGraphs = new List<GenericHelper<TVertexValue, TEdgeValue>.Graph>();

			if(!verticies.Any())
			{
				return resultSubGraphs;
			}

			var seenQueue = new Queue<TVertex>();
			var seenVerticies = new HashSet<TVertex>();
			var seenEdges = new HashSet<TEdge>();

			seenQueue.Enqueue(verticies.First());

			while(seenQueue.Any())
			{
				var current = seenQueue.Dequeue();
				seenVerticies.Add(current);
				verticies.Remove(current);

				// outgoing
				var outgoingEdges = edges.Where(e => e.From.Equals(current)).ToList();
				foreach(var edge in outgoingEdges)
				{
					if(seenVerticies.Contains(edge.To))
						continue;
					edges.Remove(edge);
					seenEdges.Add(edge);
					seenQueue.Enqueue(edge.To);
				}
				// incomming
				var incomingEdges = edges.Where(e => e.To.Equals(current)).ToList();
				foreach(var edge in incomingEdges)
				{
					if(seenVerticies.Contains(edge.From))
						continue;
					edges.Remove(edge);
					seenEdges.Add(edge);
					seenQueue.Enqueue(edge.From);
				}
			}

			if(verticies.Any())
				resultSubGraphs.AddRange(SortUtil(verticies, edges));

			var sub_verticies = seenVerticies.Select(x => new GenericHelper<TVertexValue, TEdgeValue>.Vertex(x.Value)).ToList();
			var sub_edges = seenEdges.Select(e => new GenericHelper<TVertexValue, TEdgeValue>.Edge(
						sub_verticies.First(v => v.Value.Equals(e.From.Value)),
						sub_verticies.First(v => v.Value.Equals(e.To.Value)),
						e.Value)).ToList();

			resultSubGraphs.Add(new GenericHelper<TVertexValue, TEdgeValue>.Graph(sub_verticies, sub_edges));
			return resultSubGraphs;
		}

		public List<GenericHelper<TVertexValue, TEdgeValue>.Graph> Sort()
		{
			return SortUtil(SourceGraph.Vertices.ToList(), SourceGraph.Edges.ToList());
		}
	}
}
