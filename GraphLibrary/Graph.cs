﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Immutability;
using GraphLibrary.Interfaces;

namespace GraphLibrary
{

    [Immutable]
    public class Graph<TVertex, TEdge, TVertexValue, TEdgeValue>
            : IGraph<TVertex, TEdge, TVertexValue, TEdgeValue>
        where TVertex : IVertex<TVertexValue>
        where TEdge : IEdge<TEdgeValue, TVertex, TVertexValue>
    {
        public IEnumerable<TVertex> Vertices { get; }
        public IEnumerable<TEdge> Edges { get; }

        public Graph()
        {
            Vertices = new List<TVertex>();
            Edges = new List<TEdge>();
        }

        public Graph(IEnumerable<TVertex> vertices, IEnumerable<TEdge> edges, bool verticesUnique = true, bool edgesUnique = true)
        {
            if (verticesUnique && vertices.Count() != vertices.Distinct().Count())
                throw new ArgumentException("Vertices are not unique.");
            if (edgesUnique && edges.Count() != edges.Distinct().Count())
                throw new ArgumentException("Edges are not unique.");

            Vertices = new ReadOnlyCollection<TVertex>(vertices.ToArray());
            Edges = new ReadOnlyCollection<TEdge>(edges.ToArray());
        }

        public virtual TVertex VertexOfValue(TVertexValue value) => Vertices.FirstOrDefault(v => v.Value.Equals(value));

        public virtual IEnumerable<TVertex> VerticesConnectedTo(TVertex vertex) => IncomingEdges(vertex).Select(e => e.From);
        public virtual IEnumerable<TVertex> VerticesConnectedFrom(TVertex vertex) => OutgoingEdges(vertex).Select(e => e.To);

        public virtual IEnumerable<TEdge> IncomingEdges(TVertex vertex) => Edges.Where(e => e.To.Equals(vertex));
        public virtual IEnumerable<TEdge> OutgoingEdges(TVertex vertex) => Edges.Where(e => e.From.Equals(vertex));

        private IEnumerable<TVertex> TraverseVertexDepthFirstSearch_Util(TVertex startVertex, HashSet<TVertex> visited)
        {
			visited.Add(startVertex);
			yield return startVertex;

			var children = OutgoingEdges(startVertex).Select(e => e.To).Where(v => !visited.Contains(v)).ToList();
            if (children == null)
                yield break;
            foreach (var child in children)
            {
				foreach(var subChildren in TraverseVertexDepthFirstSearch_Util(child, visited))
					yield return subChildren;
            }
        }


        public virtual IEnumerable<TVertex> TraverseVertexDepthFirstSearch(TVertex startVertex)
        {
			return TraverseVertexDepthFirstSearch_Util(startVertex, new HashSet<TVertex>());
        }

        public virtual IEnumerable<TVertex> TraverseVertexBFS(TVertex startVertex, bool directional = true)
        {
			var seen = new Queue<TVertex>();
			var visited = new HashSet<TVertex>();

			while(startVertex != null)
			{
				yield return startVertex;
				visited.Add(startVertex);

				var seenAtThisLevel = new HashSet<TVertex>();

				bool HasNotSeenYet(TVertex vertex) => !seenAtThisLevel.Contains(vertex) && !visited.Contains(vertex) && !seen.Contains(vertex);

				var allOutgoing = OutgoingEdges(startVertex).Select(e => e.To).Where(HasNotSeenYet);
				foreach(var outgoing in allOutgoing)
					seenAtThisLevel.Add(outgoing);

				if(directional)
				{
					var allIncomming = IncomingEdges(startVertex).Select(e => e.From).Where(HasNotSeenYet);
					foreach(var incoming in allIncomming)
						seenAtThisLevel.Add(incoming);
				}

				// add the seen at the level to the seen queue.
				foreach(var seenRightNow in seenAtThisLevel)
					seen.Enqueue(seenRightNow);

				if(!seen.Any())
					yield break;
				startVertex = seen.Dequeue();
			}
        }


        // public virtual object Clone()
        // {
        //     var vertices = new List<TVertex>(Vertices.Select(v => (TVertex)v.Clone()));
        //     var edges = new List<TEdge>(
        //         Edges.Select(e => (TEdge)e.CloneWithNewReferences(
        //             vertices.Single(v => v.Value.Equals(e.From.Value)),
        //             vertices.Single(v => v.Value.Equals(e.From.Value))
        //     )));
        //     return new Graph<TVertex, TEdge, TVertexValue, TEdgeValue>(vertices, edges);
        // }
    }
}
