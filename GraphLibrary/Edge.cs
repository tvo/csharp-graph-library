﻿using System;
using System.Collections.Generic;
using System.Linq;
using Immutability;
using GraphLibrary.Interfaces;

namespace GraphLibrary
{
    [Immutable]
    public class Edge<TEdgeValue, TVertex, TVertexValue> : IEdge<TEdgeValue, TVertex, TVertexValue>
        where TVertex : IVertex<TVertexValue>
    {
        public TVertex From { get; }
        public TVertex To { get; }
        public TEdgeValue Value { get; }

        public Edge(TVertex from, TVertex to, TEdgeValue value = default(TEdgeValue))
        {
            From = from;
            To = to;
            Value = value;
        }

        public override bool Equals(object obj)
        {
            if (obj is Edge<TEdgeValue, TVertex, TVertexValue>)
            {
                var other = obj as Edge<TEdgeValue, TVertex, TVertexValue>;
                return From.Equals(other.From) && To.Equals(other.To) && Value.Equals(other.Value) /*&& HasMany == other.HasMany*/;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return (this.Value?.GetHashCode() ?? 0 + this.From.GetHashCode() + this.To.GetHashCode()) % int.MaxValue;
        }

        public object Clone() => MemberwiseClone();

        public override string ToString() => $"{From.ToString()}->{To.ToString()}";

    }
}
