﻿using System;
using System.Collections.Generic;

namespace GraphLibrary.Interfaces
{
    /// <summary>
    /// A class representation in a graph context.
    /// </summary>
    public interface IMergable<in T>
    {
        void Merge(T other);
    }
}
