namespace GraphLibrary.Tests.StringGraph
{
    public class SEdge : GraphLibrary.Edge<string, SVertex, string>
    {
        public SEdge(SVertex from, SVertex to, string value = null) : base(from, to, value)
        {
        }
    }
}
