using System;
using System.Collections.Generic;

namespace GraphLibrary.Tests.StringGraph
{
    public class SPath : GraphLibrary.Tree<STree, SVertex, SEdge, string, string>
    {
        public SPath(SVertex root, IEnumerable<ITuple<SEdge, STree>> children) : base(root, children)
        {
        }

    }
}