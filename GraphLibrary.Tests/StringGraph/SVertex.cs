namespace GraphLibrary.Tests.StringGraph
{
    public class SVertex : GraphLibrary.Vertex<string>
    {
        public SVertex(string value) : base(value)
        {
        }
    }
}