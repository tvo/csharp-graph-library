using System.Collections.Generic;

namespace GraphLibrary.Tests.StringGraph
{
    public class STree : GraphLibrary.Tree<STree, SVertex, SEdge, string, string>
    {
        public STree(SVertex root, IEnumerable<ITuple<SEdge, STree>> children = null) : base(root, children)
        {
        }
    }
}