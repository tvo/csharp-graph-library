using System.Collections.Generic;

namespace GraphLibrary.Tests.StringGraph
{
    public class SGraph : GraphLibrary.Graph<SVertex, SEdge, string, string>
    {
        public SGraph(IEnumerable<SVertex> vertices, IEnumerable<SEdge> edges) : base(vertices, edges, false, false)
        {
        }
    }
}
