using Xunit;
using FluentAssertions;

using GraphLibrary.Algorithms.DisconnectedGraphLibrary;

namespace GraphLibrary.Tests.GraphSchemaTests
{
	using TVertex = GenericHelper<string, int>.Vertex;
	using TGraph = GenericHelper<string, int>.Graph;
	using TEdge = GenericHelper<string, int>.Edge;

	public class DisconnectedGraphLibrary_Tests
	{
		[Fact]
		public void SingleVertex()
		{
			// Given
			var verticies = new []{ 
				new TVertex("a")
			};
			var edges = new TEdge[]{};
			var graph = new TGraph(verticies, edges);

			// When
			var helper = new DisconnectedGraphLibrary<TGraph, TVertex, TEdge, string, int>(graph);
			var result = helper.Sort();

			// Then
			result.Should().HaveCount(1);
		}


		[Fact]
		public void TwoVerticies_OneGraph()
		{
			// Given
			var verticies = new []{ 
				new TVertex("a"),
				new TVertex("b")
			};
			var edges = new []{
				new TEdge(verticies[0], verticies[1], 5)
			};
			var graph = new TGraph(verticies, edges);
			var helper = new DisconnectedGraphLibrary<TGraph, TVertex, TEdge, string, int>(graph);

			// When
			var subGraphs = helper.Sort();

			// Then
			subGraphs.Should().HaveCount(1);
		}

		[Fact]
		public void TwoVerticies_Disconnected_TwoGraphs()
		{
			// Given
			var verticies = new []{ 
				new TVertex("a"),
				new TVertex("b")
			};
			var edges = new TEdge[] { };
			var graph = new TGraph(verticies, edges);
			var helper = new DisconnectedGraphLibrary<TGraph, TVertex, TEdge, string, int>(graph);

			// When
			var subGraphs = helper.Sort();

			// Then
			subGraphs.Should().HaveCount(2);
		}

		[Fact]
		public void SeveralNodes_OneSubGraph()
		{
			// Given
			var verticies = new []{ 
				new TVertex("a"), // 1
				new TVertex("b"), // 2
				new TVertex("c"), // 3
				new TVertex("d"), // 4
				new TVertex("e"), // 5
				new TVertex("f")  // 6
			};
			var edges = new []{
				new TEdge(verticies[0], verticies[1], 7),
				new TEdge(verticies[0], verticies[5], 14),
				new TEdge(verticies[0], verticies[2], 9),
				new TEdge(verticies[1], verticies[2], 10),
				new TEdge(verticies[1], verticies[3], 15),
				new TEdge(verticies[2], verticies[5], 2),
				new TEdge(verticies[5], verticies[4], 9),
				new TEdge(verticies[4], verticies[3], 6),
				new TEdge(verticies[2], verticies[3], 11)
			};
			var graph = new TGraph(verticies, edges);

			// When
			var subGraphs = new DisconnectedGraphLibrary<TGraph, TVertex, TEdge, string, int>(graph).Sort();

			// Then
			subGraphs.Should().HaveCount(1);
		}
	}
}
