using System.Linq;
using FluentAssertions;
using Xunit;

using TVertex = GraphLibrary.GenericHelper<string, int>.Vertex;
using TGraph = GraphLibrary.GenericHelper<string, int>.Graph;
using TEdge = GraphLibrary.GenericHelper<string, int>.Edge;

namespace GraphLibrary.Tests
{
	public class BasicFunctionalityTests
	{
		[Fact]
		public void Constructor_PutsVertexAndEdgesInCollections()
		{
			var vertexList = new []{"a", "b"}.Select(x => new TVertex(x)).ToList();
			var edgeList = new []{new TEdge(vertexList[0], vertexList[1], 1)};
			
			var graph = new TGraph(vertexList, edgeList);
			
			graph.Edges.Should().NotBeEmpty();
			graph.Vertices.Should().NotBeEmpty();
		}

		[Fact]
		public void A_HasAOutGoingEdgeTo_B()
		{
			// Given
			var vertexList = new []{"a", "b"}.Select(x => new TVertex(x)).ToList();
			var edgeList = new []{new TEdge(vertexList[0], vertexList[1], 1)};
			var graph = new TGraph(vertexList, edgeList);
			var a = graph.VertexOfValue("a");
			var b = graph.VertexOfValue("b");

			// When
			var outgoingEdges = graph.OutgoingEdges(a);

			// Then
			outgoingEdges.Should().NotBeEmpty();
		}

	}
}
