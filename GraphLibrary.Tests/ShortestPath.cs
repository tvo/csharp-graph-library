using Xunit;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;

using TVertex = GraphLibrary.GenericHelper<string, int>.Vertex;
using TGraph = GraphLibrary.GenericHelper<string, int>.Graph;
using TEdge = GraphLibrary.GenericHelper<string, int>.Edge;
using TPath = GraphLibrary.GenericHelper<string, int>.Path;

namespace GraphLibrary.Tests.GraphSchemaTests
{
	public class ShortestPath_Dijkstras_Tests
	{
		[Fact]
		public void Test1()
		{
			var verticies = new []{ 
				new TVertex("a"),
				new TVertex("b")
			};

			var edges = new []{
				new TEdge(verticies[0], verticies[1], 5)
			};
			
			var graph = new TGraph(verticies, edges);

			graph.Vertices.Should().HaveCount(2);
			graph.Edges.Should().HaveCount(1);

			var helper = new GraphLibrary.Algorithms.ShortestPath.Dijkstra(graph);
			var pathResult = helper.FindShortestPathDistance("a", "b");

			pathResult.Should().Be(5);
		}


		[Fact]
		public void PathIsOfCorrectLength()
		{
			var verticies = new []{ 
				new TVertex("a"),
				new TVertex("b")
			};

			var edges = new []{
				new TEdge(verticies[0], verticies[1], 5)
			};
			
			var graph = new TGraph(verticies, edges);

			graph.Vertices.Should().HaveCount(2);
			graph.Edges.Should().HaveCount(1);

			var helper = new GraphLibrary.Algorithms.ShortestPath.Dijkstra(graph);
			var pathResult = helper.FindShortestPath("a", "b");
			pathResult.Single();
		}


		[Fact]
		public void Example1()
		{
			var verticies = new []{ 
				new TVertex("a"), // 1
				new TVertex("b"), // 2
				new TVertex("c"), // 3
				new TVertex("d"), // 4
				new TVertex("e"), // 5
				new TVertex("f")  // 6
			};

			var edges = new []{
				new TEdge(verticies[0], verticies[1], 7),
				new TEdge(verticies[0], verticies[5], 14),
				new TEdge(verticies[0], verticies[2], 9),
				new TEdge(verticies[1], verticies[2], 10),
				new TEdge(verticies[1], verticies[3], 15),
				new TEdge(verticies[2], verticies[5], 2),
				new TEdge(verticies[5], verticies[4], 9),
				new TEdge(verticies[4], verticies[3], 6),
				new TEdge(verticies[2], verticies[3], 11)
			};
			
			var graph = new TGraph(verticies, edges);

			var helper = new GraphLibrary.Algorithms.ShortestPath.Dijkstra(graph);
			var pathResult = helper.FindShortestPath("a", "f");
			pathResult.Should().HaveCount(2);
		}
	}
}
