using Xunit;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using GraphLibrary.Tests.StringGraph;

namespace NoData.Tests.GraphSchemaTests
{
    public class TreeTests
    {
        SGraph graph;

        public TreeTests()
        {
            KeyValuePair<string, SVertex> vertexPair(string value) => new KeyValuePair<string, SVertex>(value, new SVertex(value));
            var verticies = new []{
                vertexPair("Grandpa"),
                vertexPair("Grandma"),
                vertexPair("Father"),
                vertexPair("Mother"),
                vertexPair("Brother"),
                vertexPair("Sister"),
                vertexPair("Grandchild"),
            }.ToDictionary(x => x.Key, x => x.Value);
            
            KeyValuePair<string, SEdge> edgePair(string value, SVertex from, SVertex to) 
                => new KeyValuePair<string, SEdge>(value, new SEdge(from, to, value));
            var edges = new []{
                edgePair("grandpa_father", verticies["Grandpa"], verticies["Father"]),
                edgePair("grandpa_mother", verticies["Grandpa"], verticies["Mother"]),
                edgePair("grandma_father", verticies["Grandma"], verticies["Father"]),
                edgePair("grandma_mother", verticies["Grandma"], verticies["Mother"]),
                
                edgePair("father_son", verticies["Father"], verticies["Brother"]),
                edgePair("father_daughter", verticies["Father"], verticies["Sister"]),
                edgePair("mother_son", verticies["Mother"], verticies["Brother"]),
                edgePair("mother_daughter", verticies["Mother"], verticies["Sister"]),

                edgePair("mother_grandchild", verticies["Sister"], verticies["Grandchild"]),
            }.ToDictionary(x => x.Key, x => x.Value);
            
            graph = new SGraph(verticies.Values, edges.Values);
        }

        [Fact]
        public void Tree_Traverse_Success()
        {
            var father = graph.VertexOfValue("Father");
            var brother = graph.VertexOfValue("Brother");
            var sister = graph.VertexOfValue("Sister");

            var son = graph.Edges.Single(e => e.From == father && e.To == brother);
            var daughter = graph.Edges.Single(e => e.From == father && e.To == sister);

            var tree = new STree(father,
                new[] {
                    GraphLibrary.ITuple.Create(son, new STree(brother)),
                    GraphLibrary.ITuple.Create(daughter, new STree(sister)),
                }
            );
            var traversedEdges = new List<SEdge>();
            tree.TraverseDepthFirstSearch(e => traversedEdges.Add(e));
            Assert.Equal(2, traversedEdges.Count);
        }

        [Fact]
        public void Tree_Flatten_Success()
        {
            var father = graph.VertexOfValue("Father");
            var brother = graph.VertexOfValue("Brother");
            var sister = graph.VertexOfValue("Sister");

            var son = graph.Edges.Single(e => e.From == father && e.To == brother);
            var daughter = graph.Edges.Single(e => e.From == father && e.To == sister);

            var tree = new STree(father,
                new[] {
                    GraphLibrary.ITuple.Create(son, new STree(brother)),
                    GraphLibrary.ITuple.Create(daughter, new STree(sister)),
                }
            );

            var flat = tree.Flatten();
            flat.Vertices.Should().HaveCount(3);
            flat.Edges.Should().HaveCount(2);
            Assert.NotNull(flat.VertexOfValue(father.Value));
            Assert.NotNull(flat.VertexOfValue(brother.Value));
            Assert.NotNull(flat.VertexOfValue(sister.Value));
        }

        [Fact]
        public void Tree_TraverseVertexEdges_Success()
        {
            var father = graph.VertexOfValue("Father");
            var brother = graph.VertexOfValue("Brother");
            var sister = graph.VertexOfValue("Sister");

            var son = graph.Edges.Single(e => e.From == father && e.To == brother);
            var daughter = graph.Edges.Single(e => e.From == father && e.To == sister);

            var tree = new STree(father,
                new[] {
                    GraphLibrary.ITuple.Create(son, new STree(brother)),
                    GraphLibrary.ITuple.Create(daughter, new STree(sister)),
                }
            );

            var list = new List<SVertex>();
            tree.TraverseDepthFirstSearch((SVertex v, IEnumerable<SEdge> c) => list.Add(v));

            Assert.Equal(3, list.Count);

            list[0].Should().Be(father);
            list[1].Should().Be(brother);
            list[2].Should().Be(sister);
        }
    }
}
