using Xunit;
using System.Linq;
using FluentAssertions;

using TVertex = GraphLibrary.GenericHelper<string, int>.Vertex;
using TGraph = GraphLibrary.GenericHelper<string, int>.Graph;
using TEdge = GraphLibrary.GenericHelper<string, int>.Edge;

namespace GraphLibrary.Tests.MinimumSpanningTests
{
	public class MinimumSpanningTree_Krushkal_Tests
	{
		[Fact]
		public void BaseCase_TwoVertices_EdgeResultHasCorrectCost()
		{
			var verticies = new []{ 
				new TVertex("a"),
				new TVertex("b")
			};

			var edges = new []{
				new TEdge(verticies[0], verticies[1], 5)
			};
			
			var graph = new TGraph(verticies, edges);

			graph.Vertices.Should().HaveCount(2);
			graph.Edges.Should().HaveCount(1);

			var helper = new GraphLibrary.Algorithms.MinimumSpanningTree.Krushkal(graph);
			var sub = helper.MinSpanningTree();

			sub.Edges.First().Value.Should().Be(5);
		}


		[Fact]
		public void BaseCase_TwoVertices_OnlyReturnsOneEdge()
		{
			var verticies = new []{ 
				new TVertex("a"),
				new TVertex("b")
			};

			var edges = new []{
				new TEdge(verticies[0], verticies[1], 5)
			};
			
			var graph = new TGraph(verticies, edges);

			graph.Vertices.Should().HaveCount(2);
			graph.Edges.Should().HaveCount(1);

			var helper = new GraphLibrary.Algorithms.MinimumSpanningTree.Krushkal(graph);
			var sub = helper.MinSpanningTree();
			sub.Edges.Single();
		}


		[Fact]
		public void SmallGraph_CorrectVertexCount()
		{
			var verticies = new []{ 
				new TVertex("a"), // 0
				new TVertex("b"), // 1
				new TVertex("c"), // 2
				new TVertex("d"), // 3
				new TVertex("e"), // 4
				new TVertex("f")  // 5
			};

			var edges = new []{
				new TEdge(verticies[0], verticies[1], 2),
				new TEdge(verticies[0], verticies[5], 4),
				new TEdge(verticies[1], verticies[2], 6),
				new TEdge(verticies[1], verticies[5], 5),
				new TEdge(verticies[2], verticies[5], 1),
				new TEdge(verticies[2], verticies[3], 3),
				new TEdge(verticies[3], verticies[4], 2),
				new TEdge(verticies[4], verticies[5], 5)
			};
			
			var graph = new TGraph(verticies, edges);

			var helper = new GraphLibrary.Algorithms.MinimumSpanningTree.Krushkal(graph);
			var sub = helper.MinSpanningTree();
			sub.Vertices.Should().HaveCount(6);
		}
	}
}
