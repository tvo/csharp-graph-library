﻿using System;
using System.Threading.Tasks;
using CommandLine;
using Autofac;
using Serilog;
using Console.Output;
using Console.Output.Webgraphviz;

namespace Console
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .CreateLogger();

            try
            {
                var builder = new ContainerBuilder();
                builder.RegisterModule<DependencyInjection>();
                var container = builder.Build();

                var program = new Program(container);
                await program.Execute(args);
            }
            catch (Exception e)
            {
                Log.Logger.Error("Fatal exception: {0}", e.ToString());
            }
        }

        private IContainer DiContainer { get; }


        public Program(IContainer diContainer)
        {
            DiContainer = diContainer;
        }

        public async Task Execute(string[] args)
        {
            var parser = CommandLine.Parser.Default;

            await parser.ParseArguments<Verbs.ReadOptions>(args)
                .MapResult(FromFile,
                errs => Task.FromResult(0))
                ;
        }

        private async Task FromFile(Verbs.ReadOptions options)
        {
            Reader.IInput reader;
            var path = options.Path;
            if (string.IsNullOrWhiteSpace(path))
                reader = new Reader.StdInput();
            else reader = new Reader.FileInput(options.Path);

            reader = new Reader.LogInput(reader, DiContainer.Resolve<ILogger>());

            var lines = await reader.GetInputAsync();

            await Process(lines, new WebGraphvizOutput());
        }

        private async Task Process(string[] lines, IOutputWritter output)
        {
            var repository = DiContainer.Resolve<Reader.GraphReaderRepository.IGraphReaderRepository>();
            var parsedLines = await repository.Read(lines);
            if (parsedLines is null) return;
            var graph = repository.ParseToGraph(parsedLines);
            await output.Output(graph);
        }
    }
}
