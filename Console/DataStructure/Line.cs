using System.Collections.Generic;

namespace Console.DataStructure
{
    public class InputGraphLine
    {
        public static IReadOnlyList<string> Delimeters = new List<string>()
        {
            ">",
            "=>",
            "->",
            "_>",
        }.AsReadOnly();

        public InputGraphLine(string from, string to)
        {
            From = from;
            To = to;
        }

        public string From { get; }
        public string To { get; }
    }
}