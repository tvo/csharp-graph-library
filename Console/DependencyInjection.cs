using Autofac;
using Serilog;
using AutofacSerilogIntegration;

namespace Console
{
    public class DependencyInjection : Module
    {
        public DependencyInjection() : base()
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Reader.GraphReaderRepository.GraphReaderRepository>().As<Reader.GraphReaderRepository.IGraphReaderRepository>();
            builder.RegisterDecorator<Reader.GraphReaderRepository.LogGraphReaderRepository, Reader.GraphReaderRepository.IGraphReaderRepository>();

            // builder.RegisterType<Reader.PngGraphWriter>().As<Reader.Outputs.IOutputWritter>();
            builder.RegisterLogger();
        }
    }
}