using System.Collections.Generic;
using System.Threading.Tasks;

namespace Console.Reader
{
    public class StdInput : IInput
    {
        Task<string[]> IInput.GetInputAsync()
        {
            string s;
            var result = new List<string>();
            while ((s = System.Console.ReadLine()) != null)
            {
                result.Add(s);
            }

            return Task.FromResult(result.ToArray());
        }
    }
}