using System;
using System.IO;

namespace Console.Reader
{
    public class FileInput : IInput
    {
        private string filePath { get; }

        public FileInput(string filePath)
        {
            this.filePath = filePath;
        }

        public async System.Threading.Tasks.Task<string[]> GetInputAsync()
        {
            if (!File.Exists(filePath))
                return null;

            // file to lines of text
            var lines = new string[] { };
            using (var reader = File.OpenText(filePath))
            {
                var fileText = await reader.ReadToEndAsync();
                lines = fileText.Split(new[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            }

            return lines;
        }
    }
}