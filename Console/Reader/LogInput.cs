using System.Threading.Tasks;
using Serilog;

namespace Console.Reader
{
    public class LogInput : IInput
    {
        private IInput Repository { get; }
        private ILogger Log { get; }

        public LogInput(IInput repository, ILogger log)
        {
            Repository = repository;
            Log = log;
        }

        public async Task<string[]> GetInputAsync()
        {
            var lines = await Repository.GetInputAsync();

            if (lines is null)
                Log.Error("Could not read input.");
            else
                Log.Information("Finished reading input: {0} lines.", lines.Length);
            return lines;
        }
    }
}