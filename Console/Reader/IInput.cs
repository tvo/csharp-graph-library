using System.Threading.Tasks;

namespace Console.Reader
{
    public interface IInput
    {
         Task<string[]> GetInputAsync();
    }
}