using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GraphType = GraphLibrary.GenericHelper<string, string>;

namespace Console.Reader.GraphReaderRepository
{
    public class GraphReaderRepository : IGraphReaderRepository
    {
        public GraphReaderRepository()
        {
        }

        public Task<List<DataStructure.InputGraphLine>> Read(string[] lines)
        {
            // splits out the lines of text into the data structure {from, to} format.
            var result = new List<DataStructure.InputGraphLine>();
            foreach (var line in lines)
            {
                var parts = line.Split(DataStructure.InputGraphLine.Delimeters.ToArray(), StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length != 2)
                    continue;
                result.Add(new DataStructure.InputGraphLine(parts[0].Trim(), parts[1].Trim()));
            }

            return Task.FromResult(result);
        }

        public GraphType.Graph ParseToGraph(IEnumerable<DataStructure.InputGraphLine> input)
        {
            var nodes = input.Select(x => new string[] { x.From, x.To }).SelectMany(x => x).Distinct().ToList();

            var vertices = new Dictionary<string, GraphType.Vertex>();
            var edges = new List<GraphType.Edge>();

            foreach (var line in input)
            {
                if (!vertices.Keys.Any(x => x.Equals(line.From)))
                    vertices.Add(line.From, new GraphType.Vertex(line.From));
                if (!vertices.Keys.Any(x => x.Equals(line.To)))
                    vertices.Add(line.To, new GraphType.Vertex(line.To));
                string edgeValue = null;
                edges.Add(new GraphType.Edge(vertices[line.From], vertices[line.To], edgeValue));
            }

            return new GraphType.Graph(vertices.Values, edges);
        }

    }
}