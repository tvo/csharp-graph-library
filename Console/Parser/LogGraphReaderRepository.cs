using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;
using GraphType = GraphLibrary.GenericHelper<string, string>;

namespace Console.Reader.GraphReaderRepository
{
    public class LogGraphReaderRepository : IGraphReaderRepository
    {
        private IGraphReaderRepository Reposiotory { get; }
        private ILogger logger { get; }

        public LogGraphReaderRepository(ILogger log, IGraphReaderRepository repository)
        {
            Reposiotory = repository;
        }

        public async Task<List<DataStructure.InputGraphLine>> Read(string[] lines)
        {
            try
            {
                var result = await Reposiotory.Read(lines);
                if (result is null)
                    Log.Error("Could not parse from : {0}", string.Join(" ", lines));
                return result;
            }
            catch (Exception ex)
            {
                Log.Information("Error: {0}", ex.ToString());
                return new List<DataStructure.InputGraphLine>();
            }
        }

        public GraphType.Graph ParseToGraph(IEnumerable<DataStructure.InputGraphLine> input)
        {
            try
            {
                var graph = Reposiotory.ParseToGraph(input);

                if (graph is null)
                    Log.Error("Could not parse data into a graph.");
                else
                {
                    Log.Information("Created Graph: {0} nodes and {1} edges", graph.Vertices.Count(), graph.Edges.Count());
                    Log.Information("Vertices: {0}", string.Join(", ", graph.Vertices.Select(x => x.Value).ToArray()));
                }

                return graph;
            }
            catch (Exception ex)
            {
                Log.Information("Error: {0}", ex.ToString());
                return null;
            }
        }
    }
}