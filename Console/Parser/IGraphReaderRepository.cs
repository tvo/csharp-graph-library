using System.Collections.Generic;
using System.Threading.Tasks;
using GraphLibrary;
using GraphType = GraphLibrary.GenericHelper<string, string>;

namespace Console.Reader.GraphReaderRepository
{
    public interface IGraphReaderRepository
    {
        Task<List<DataStructure.InputGraphLine>> Read(string[] lines);
        GraphType.Graph ParseToGraph(IEnumerable<DataStructure.InputGraphLine> input);
    }
}