using System.Threading.Tasks;
using GraphType = GraphLibrary.GenericHelper<string, string>;

namespace Console.Output
{
    public interface IOutputWritter
    {
        Task Output(GraphType.Graph graph);
    }
}