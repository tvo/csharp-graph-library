using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GraphType = GraphLibrary.GenericHelper<string, string>;

namespace Console.Output.Webgraphviz
{
    public class WebGraphvizOutput : IOutputWritter
    {
        public Task Output(GraphType.Graph graph)
        {
            var sb = new StringBuilder();

            var edges = graph.Edges.Select(x => $"\"{x.From.Value}\" -> {x.To.Value}").ToList();

            sb.Append("digraph G {" + System.Environment.NewLine + "\t");
            sb.Append(string.Join(System.Environment.NewLine + "\t", edges));
            sb.Append(System.Environment.NewLine + "}");
            System.Console.WriteLine(sb.ToString());

            return Task.CompletedTask;
        }
    }
}