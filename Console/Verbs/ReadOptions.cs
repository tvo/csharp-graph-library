using CommandLine;

namespace Console.Verbs
{
    public class ReadOptions
    {
        [Option('p', "path", Required = false, HelpText = "This is the path of the input csv file to read the graph data from.")]
        public string Path { get; set; }

        [Option('v', "verbose", Required = false, HelpText = "Extra logging to std.")]
        bool Verbose { get; set; }
    }
}